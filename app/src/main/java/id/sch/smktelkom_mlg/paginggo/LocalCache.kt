package id.sch.smktelkom_mlg.paginggo

import android.arch.paging.DataSource
import id.sch.smktelkom_mlg.paginggo.Database.ProdukDao
import id.sch.smktelkom_mlg.paginggo.Model.ProdukModel
import java.util.concurrent.Executor

class LocalCache(val produkDao: ProdukDao, val executor: Executor) {
    fun insert(repos: List<ProdukModel>) {
        executor.execute {
            produkDao.insert(repos)
        }
    }

    fun getData(name: String): DataSource.Factory<Int, ProdukModel> {
        return produkDao.getProduk("%${name.replace(' ', '%')}%")
    }
}