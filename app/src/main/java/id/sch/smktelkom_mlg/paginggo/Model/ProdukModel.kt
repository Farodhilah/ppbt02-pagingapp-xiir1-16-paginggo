package id.sch.smktelkom_mlg.paginggo.Model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "produk")
data class ProdukModel(
    @PrimaryKey @field:SerializedName("Id") val Id: Int,
    @field:SerializedName("Nama_produk") val Nama_produk: String,
    @field:SerializedName("Nama_perusahaan") val Nama_perusahaan: String
)