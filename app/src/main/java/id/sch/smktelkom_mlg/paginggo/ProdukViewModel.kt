package id.sch.smktelkom_mlg.paginggo

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import android.arch.lifecycle.ViewModel
import android.arch.paging.PagedList
import id.sch.smktelkom_mlg.paginggo.Model.ProdukModel
import id.sch.smktelkom_mlg.paginggo.Model.ProdukResult

class ProdukViewModel(val repository: ProdukRepository) : ViewModel() {
    private val queryLiveData = MutableLiveData<String>()
    private val repoResult: LiveData<ProdukResult> = Transformations.map(queryLiveData, {
        repository.search(it)
    })

    val repos: LiveData<PagedList<ProdukModel>> = Transformations.switchMap(repoResult,
        { it -> it.data })
    val networkErrors: LiveData<String> = Transformations.switchMap(repoResult,
        { it -> it.networkErrors })

    fun searchRepo(queryString: String) {
        queryLiveData.postValue(queryString)
    }
}