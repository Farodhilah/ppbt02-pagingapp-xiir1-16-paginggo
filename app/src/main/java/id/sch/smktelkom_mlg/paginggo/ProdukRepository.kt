package id.sch.smktelkom_mlg.paginggo

import android.arch.paging.LivePagedListBuilder
import id.sch.smktelkom_mlg.paginggo.Model.ProdukResult

class ProdukRepository(val api: Api, val cache: LocalCache) {
    fun search(query: String): ProdukResult {
        val dataSourceFactory = cache.getData(query)
        val boundaryCallback = BoundaryCallback(api, cache, query)
        val data = LivePagedListBuilder(dataSourceFactory, 10)
            .setBoundaryCallback(boundaryCallback)
            .build()
        return ProdukResult(data, boundaryCallback.networkErrors)
    }
}
