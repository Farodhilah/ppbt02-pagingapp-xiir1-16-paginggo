package id.sch.smktelkom_mlg.paginggo

import android.util.Log
import id.sch.smktelkom_mlg.paginggo.Model.ProdukModel
import id.sch.smktelkom_mlg.paginggo.Model.ProdukResponse
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

fun getProduk(
    service: Api,
    page: Int,
    query: String,
    onSuccess: (repos: List<ProdukModel>) -> Unit,
    onError: (error: String) -> Unit
) {

    Log.d("API", service.getProduk(page, query).toString())
    service.getProduk(page, query).enqueue(

        object : Callback<ProdukResponse> {
            override fun onFailure(call: Call<ProdukResponse>?, t: Throwable) {
                onError(t.message ?: "unknown error")
            }

            override fun onResponse(
                call: Call<ProdukResponse>?,
                response: Response<ProdukResponse>
            ) {
                if (response.isSuccessful) {
                    val repos = response.body()?.Data ?: emptyList()
                    onSuccess(repos)
                } else {
                    onError(response.errorBody()?.string() ?: "Unknown error")
                }
            }
        }
    )
}

interface Api {

    @GET("/produks")
    fun getProduk(@Query("page") page: Int, @Query("search") search: String): Call<ProdukResponse>

    companion object {
        fun getService(ip: String): Api {
            val logger = HttpLoggingInterceptor()
            logger.level = HttpLoggingInterceptor.Level.BASIC

            val client = OkHttpClient.Builder()
                .addInterceptor(logger)
                .build()
            val retrofit = Retrofit.Builder().baseUrl("http://" + ip)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            return retrofit.create(Api::class.java)
        }
    }

}