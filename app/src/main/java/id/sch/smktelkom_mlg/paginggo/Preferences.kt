package id.sch.smktelkom_mlg.paginggo

enum class Preferences(val value: String) {
    PREFERENCES_NAME("go_preferences"),
    IP("IP_address"),
    PORT("Port")
}