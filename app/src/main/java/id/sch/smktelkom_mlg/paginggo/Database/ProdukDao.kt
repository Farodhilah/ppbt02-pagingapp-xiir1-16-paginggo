package id.sch.smktelkom_mlg.paginggo.Database

import android.arch.paging.DataSource
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import id.sch.smktelkom_mlg.paginggo.Model.ProdukModel

@Dao
interface ProdukDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(posts: List<ProdukModel>)

    @Query("SELECT * FROM produk where (Nama_produk LIKE :queryString) ORDER BY Nama_produk ASC")
    fun getProduk(queryString: String): DataSource.Factory<Int, ProdukModel>
}