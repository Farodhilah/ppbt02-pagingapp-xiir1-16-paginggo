package id.sch.smktelkom_mlg.paginggo.Model

import android.arch.lifecycle.LiveData
import android.arch.paging.PagedList

data class ProdukResult(
    val data: LiveData<PagedList<ProdukModel>>,
    val networkErrors: LiveData<String>
)