package id.sch.smktelkom_mlg.paginggo.Database

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import id.sch.smktelkom_mlg.paginggo.Model.ProdukModel

@Database(entities = [ProdukModel::class], version = 1, exportSchema = false)
abstract class ProdukDatabase : RoomDatabase() {
    abstract fun produkDao(): ProdukDao

    companion object {

        @Volatile
        private var INSTANCE: ProdukDatabase? = null

        fun getInstance(context: Context): ProdukDatabase =
            INSTANCE ?: synchronized(this) {
                INSTANCE
                    ?: buildDatabase(context).also { INSTANCE = it }
            }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(
                context,
                ProdukDatabase::class.java, "db_produk.db"
            ).allowMainThreadQueries().build()
    }
}