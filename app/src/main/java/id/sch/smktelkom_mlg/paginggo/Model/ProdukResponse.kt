package id.sch.smktelkom_mlg.paginggo.Model

import com.google.gson.annotations.SerializedName

data class ProdukResponse(
    @SerializedName("NextPage") val NextPage: Int = 0,
    @SerializedName("Data") val Data: List<ProdukModel> = emptyList()
)
