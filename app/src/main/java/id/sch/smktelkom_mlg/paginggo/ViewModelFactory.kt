package id.sch.smktelkom_mlg.paginggo

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider

class ViewModelFactory(val repo: ProdukRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ProdukViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return ProdukViewModel(repo) as T
        }
        throw IllegalArgumentException("Unknown ProdukViewModel class")
    }

}
