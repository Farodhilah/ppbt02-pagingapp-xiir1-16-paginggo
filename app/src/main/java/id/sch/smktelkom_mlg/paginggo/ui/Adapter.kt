package id.sch.smktelkom_mlg.paginggo.ui

import android.arch.paging.PagedListAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.sch.smktelkom_mlg.paginggo.Model.ProdukModel
import id.sch.smktelkom_mlg.paginggo.R
import kotlinx.android.synthetic.main.layout_recycler.view.*

class Adapter : PagedListAdapter<ProdukModel, Adapter.ProdukViewHolder>(
    DiffCallback
) {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ProdukViewHolder {
        return ProdukViewHolder(
            LayoutInflater.from(p0.context).inflate(
                R.layout.layout_recycler,
                p0,
                false
            )
        )
    }

    override fun onBindViewHolder(p0: ProdukViewHolder, p1: Int) {
        val produk = getItem(p1)
        if (produk != null) {
            p0.bind(produk)
        }
    }

    companion object {
        val DiffCallback = object : DiffUtil.ItemCallback<ProdukModel>() {
            override fun areItemsTheSame(p0: ProdukModel, p1: ProdukModel): Boolean {
                return p0.Id == p1.Id
            }

            override fun areContentsTheSame(p0: ProdukModel, p1: ProdukModel): Boolean {
                return p0 == p1
            }

        }
    }

    class ProdukViewHolder(val v: View) : RecyclerView.ViewHolder(v) {
        fun bind(m: ProdukModel) {
            v.tv_name.text = m.Nama_produk
            v.tv_type.text = m.Nama_perusahaan

        }
    }
}