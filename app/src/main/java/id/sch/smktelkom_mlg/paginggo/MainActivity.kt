package id.sch.smktelkom_mlg.paginggo

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.arch.paging.PagedList
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.widget.SearchView
import android.widget.Toast
import id.sch.smktelkom_mlg.paginggo.Model.ProdukModel
import id.sch.smktelkom_mlg.paginggo.ui.Adapter
import id.sch.smktelkom_mlg.paginggo.ui.SettingActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    lateinit var monsterViewModel: ProdukViewModel
    lateinit var pref: SharedPreferences
    val adapter = Adapter()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        pref = getSharedPreferences(Preferences.PREFERENCES_NAME.value, Context.MODE_PRIVATE)

    }

    override fun onResume() {
        super.onResume()
        if (!pref.contains(Preferences.IP.value) || !pref.contains(Preferences.PORT.value)) {
            val i = Intent(this, SettingActivity::class.java)
            startActivity(i)
        } else {
            monsterViewModel = ViewModelProviders.of(
                this,
                Injection.provideViewModelFactory(this)
            )
                .get(ProdukViewModel::class.java)
            recyclerview.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
            recyclerview.layoutManager = LinearLayoutManager(this)

            recyclerview.adapter = adapter
            monsterViewModel.searchRepo("")
            monsterViewModel.repos.observe(this, Observer<PagedList<ProdukModel>> {
                adapter.submitList(it)
            })
            monsterViewModel.networkErrors.observe(this, Observer<String> {
                Toast.makeText(this, it!!, Toast.LENGTH_SHORT).show()
            })
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu!!)
        val searchItem = menu.findItem(R.id.search)
        val searchView = searchItem.actionView as SearchView
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(p0: String?): Boolean {
                recyclerview.scrollToPosition(0)
                monsterViewModel.searchRepo(p0!!)
                adapter.submitList(null)
                return false
            }

            override fun onQueryTextChange(p0: String?): Boolean {
                recyclerview.scrollToPosition(0)
                monsterViewModel.searchRepo(p0!!)
                adapter.submitList(null)
                return false
            }
        })

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.setting -> {
                val i = Intent(this, SettingActivity::class.java)
                startActivity(i)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
