package id.sch.smktelkom_mlg.paginggo

import android.arch.lifecycle.ViewModelProvider
import android.content.Context
import id.sch.smktelkom_mlg.paginggo.Database.ProdukDatabase
import java.util.concurrent.Executors


object Injection {
    private fun provideCache(context: Context): LocalCache {
        val database = ProdukDatabase.getInstance(context)
        return LocalCache(
            database.produkDao(),
            Executors.newSingleThreadExecutor()
        )
    }


    private fun provideMonsterRepository(context: Context): ProdukRepository {
        val pref = context.getSharedPreferences(Preferences.PREFERENCES_NAME.value, Context.MODE_PRIVATE)
        val ip = pref.getString(Preferences.IP.value, "")
        val port = pref.getString(Preferences.PORT.value, "")
        return ProdukRepository(
            Api.getService(
                ip + ":" + port
            ), provideCache(context)
        )
    }


    fun provideViewModelFactory(context: Context): ViewModelProvider.Factory {
        return ViewModelFactory(
            provideMonsterRepository(
                context
            )
        )
    }
}